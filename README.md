k8s-monitoring
=========

Installs Prometheus, Grafana for basic monitoring, Hubble (optionally) for traceability if Cilium is specified as the CNI.  Prometheus is installed via Helm and Operator which includes node exporter, cAdvisor, etc.

This series of roles is more purpose-specific than a general implementation like kube-spray.

Requirements
------------
Grafana requires storage so it is assumed that either local storage has been configured or [dynamic storage](https://gitlab.com/ryanamorrison-microservice-projects/kube/k8s-storage) is available and that a default storage class has been configured in the environment.

Assumes certain ansible inventory groups are present in a specific hierarchy:
```
---
firstcp:
  hosts:
    cp1:
      var_host_endpoint_ip: 192.168.1.2
      ansible_ssh_private_key_file: "{{ lookup('env', 'HOME') }}/.ssh/{{ inventory_hostname }}"
restcps:
  hosts:
    cp2:
      var_host_endpoint_ip: 192.168.1.3
      ansible_ssh_private_key_file: "{{ lookup('env', 'HOME') }}/.ssh/{{ inventory_hostname }}"
allcps:
  children:
    firstcp:
    restcps:
  vars:
    var_kube_version: 1.27
    var_kube_install_version: 1.27.1-00
    var_service_subnet_cidr: 10.244.0.0/16
    var_pod_subnet_cidr: 172.20.0.0/16
    var_node_mask_cidr: 24
    var_dns_domain: cluster.local
    var_cluster_name: kubernetes 
    var_storage: rook
    var_cni: cilium
    cilium_install_with_helm: false
    cilium_use_gw_api: false
    cilium_use_kube_proxy: true
    cilium_version: 1.14.6
    var_storage: rook
workers:
  hosts:
    worker1:
      var_host_endpoint_ip: 192.168.1.4
      ansible_ssh_private_key_file: "{{ lookup('env', 'HOME') }}/.ssh/{{ inventory_hostname }}"
    worker2:
      var_host_endpoint_ip: 192.168.1.5
      ansible_ssh_private_key_file: "{{ lookup('env', 'HOME') }}/.ssh/{{ inventory_hostname }}"
    worker3:
      var_host_endpoint_ip: 192.168.1.6
      ansible_ssh_private_key_file: "{{ lookup('env', 'HOME') }}/.ssh/{{ inventory_hostname }}"
  vars:
    var_local_storage: true
    var_localk8s_disk: vdd
allkube:
  children:
    allcps:
    workers:
  vars:
    var_kube_version: 1.27
```
Also see variables and playbook below

Role Variables
--------------
Are broken into two categories based on where they are located.
### default variables
The following two variables are sane defaults.  Override them in the inventory with `rook` and `cilium` respectively if installing one or both.
```
var_storage: "none"
var_cni: "none"
``` 

### inventory variables
each node (ec2 instance, host, QEMU vm, etc.) is assumed to have a ssh key that CI runners will use to connect to the host:
```
ansible_ssh_private_key_file: "{{ lookup('env', 'HOME') }}/.ssh/{{ inventory_hostname }}"
```

Dependencies
------------
* k8s-common
* k8s-cps-common
* k8s-init-cluster
* k8s-join-cps (optional, if needed for additional CPs)
* k8s-join-workers
* k8s-storage (optional if using local storage instead)

Also see example Playbook below.

Example Playbook
----------------
Uses the first control plane to configure local storage, freeing the runner from needing credentials to connect to the cluster, kubectl installed, etc.
```
---
- hosts: allkube
  become: true
  become_method: sudo
  include_roles:
    - k8s-common

- hosts: allcps
  become: true
  become_method: sudo
  include_roles:
    - k8s-cps-common

- hosts: firstcp
  become: true
  become_method: sudo
  include_roles:
    - k8s-init-cluster

- hosts: restcps
  serial: 1
  become: true
  become_method: sudo
  include_roles:
    - k8s-join-cps

- hosts: workers
  serial: 1
  become: true
  become_method: sudo
  include_roles:
    - k8s-join-workers

- hosts: firstcp
  become: true
  become_method: sudo
  roles:
    - k8s-storage
    - k8s-monitoring
```

License
-------

BSD

Author Information
------------------

Ryan A. Morrison (ryan@ryanamorrison.com)
